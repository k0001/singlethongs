# 0.1

* Initial version, more or less based on `singletons-2.5.1`.

* Builds with GHC 8.6.5, GHC 8.8.2, GHCJS 8.6.0.
